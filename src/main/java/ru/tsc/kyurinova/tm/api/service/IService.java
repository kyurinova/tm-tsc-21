package ru.tsc.kyurinova.tm.api.service;

import ru.tsc.kyurinova.tm.api.repository.IRepository;
import ru.tsc.kyurinova.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

    void add(E entity);

    void remove(E entity);

    List<E> findAll();

    List<E> findAll(Comparator<E> comparator);

    void clear();

    E findById(String id);

    E findByIndex(Integer index);

    E removeById(String id);

    E removeByIndex(Integer index);

    boolean existsById(String id);

    boolean existsByIndex(Integer index);

}
