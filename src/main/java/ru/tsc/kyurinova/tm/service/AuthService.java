package ru.tsc.kyurinova.tm.service;

import ru.tsc.kyurinova.tm.api.service.IAuthService;
import ru.tsc.kyurinova.tm.api.service.IUserService;
import ru.tsc.kyurinova.tm.exception.user.AccessDeniedException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyLoginException;
import ru.tsc.kyurinova.tm.exception.empty.EmptyPasswordException;
import ru.tsc.kyurinova.tm.model.User;
import ru.tsc.kyurinova.tm.util.HashUtil;

public class AuthService implements IAuthService {

    private final IUserService userService;

    private String userId;

    public AuthService(final IUserService userService) {
        this.userService = userService;
    }

    @Override
    public User getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId == null;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        final String hash = HashUtil.salt(password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void registry(String login, String password, String email) {
        userService.create(login, password, email);
    }
}
