package ru.tsc.kyurinova.tm.repository;

import ru.tsc.kyurinova.tm.api.repository.ICommandRepository;
import ru.tsc.kyurinova.tm.command.AbstractCommand;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Override
    public AbstractCommand getCommandByName(String name) {
        return commands.get(name);
    }

    @Override
    public AbstractCommand getCommandByArg(String arg) {
        return arguments.get(arg);
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @Override
    public Collection<String> getListCommandName() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : commands.values()) {
            final String name = command.name();
            if (!Optional.ofNullable(name).isPresent() || name.isEmpty()) continue;
            result.add(name);
        }
        return null;
    }

    @Override
    public Collection<String> getListCommandArg() {
        final List<String> result = new ArrayList<>();
        for (final AbstractCommand command : arguments.values()) {
            final String arg = command.name();
            if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) continue;
            result.add(arg);
        }
        return null;
    }

    @Override
    public void add(AbstractCommand command) {
        final String arg = command.arg();
        final String name = command.name();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        if (Optional.ofNullable(name).isPresent()) commands.put(name, command);
    }

}
