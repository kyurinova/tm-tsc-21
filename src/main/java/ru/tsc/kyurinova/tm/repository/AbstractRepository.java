package ru.tsc.kyurinova.tm.repository;

import ru.tsc.kyurinova.tm.api.repository.IRepository;
import ru.tsc.kyurinova.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(final E entity) {
        entities.add(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public List<E> findAll(Comparator<E> comparator) {
        entities.sort(comparator);
        return entities;
    }

    @Override
    public void clear() {
        entities.removeAll(entities);
    }

    @Override
    public E findById(final String id) {
        return entities.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElse(null);

    }

    @Override
    public E findByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final Integer index) {
        final E entity = findByIndex(index);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

    @Override
    public boolean existsById(final String id) {
        final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final Integer index) {
        final E entity = findByIndex(index);
        return entity != null;
    }
}
