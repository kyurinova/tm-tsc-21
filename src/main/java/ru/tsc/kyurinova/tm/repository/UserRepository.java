package ru.tsc.kyurinova.tm.repository;

import ru.tsc.kyurinova.tm.api.repository.IUserRepository;
import ru.tsc.kyurinova.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(String login) {
        return entities.stream()
                .filter(u -> u.getLogin().equals(login))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(String email) {
        return entities.stream()
                .filter(u -> u.getEmail().equals(email))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeById(String id) {
        final User user = findById(id);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }

    @Override
    public User removeByLogin(String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        entities.remove(user);
        return user;
    }
}
