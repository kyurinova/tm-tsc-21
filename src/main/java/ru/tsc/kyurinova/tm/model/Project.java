package ru.tsc.kyurinova.tm.model;

import ru.tsc.kyurinova.tm.api.entity.IWBS;
import ru.tsc.kyurinova.tm.enumerated.Status;

import java.util.Date;
import java.util.UUID;

public class Project extends AbstractOwnerEntity implements IWBS {

    public Project() {
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Project(String name, String description, Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    public Project(String userId, String name, String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public Project(String userId, String name, String description, Date startDate) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    private String name;

    private String description;

    private String userId;

    private Status status = Status.NOT_STARTED;

    private Date startDate;

    private Date finishDate;

    private Date created = new Date();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

}
