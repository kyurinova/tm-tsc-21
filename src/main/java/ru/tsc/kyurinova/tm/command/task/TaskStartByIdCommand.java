package ru.tsc.kyurinova.tm.command.task;

import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-start-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Start task by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().startById(userId, id);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
