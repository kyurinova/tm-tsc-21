package ru.tsc.kyurinova.tm.command;

import ru.tsc.kyurinova.tm.api.service.IServiceLocator;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    @Override
    public String toString() {
        return super.toString();
    }
}
