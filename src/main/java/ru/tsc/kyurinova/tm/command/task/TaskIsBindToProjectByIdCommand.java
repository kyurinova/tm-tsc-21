package ru.tsc.kyurinova.tm.command.task;

import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskIsBindToProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-bind-to-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Bind task to project by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (serviceLocator.getProjectService().findById(userId, projectId) == null)
            throw new ProjectNotFoundException();
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (serviceLocator.getTaskService().findById(userId, taskId) == null) throw new TaskNotFoundException();
        final Task taskUpdated = serviceLocator.getProjectTaskService().bindTaskById(userId, projectId, taskId);
        Optional.ofNullable(taskUpdated).orElseThrow(TaskNotFoundException::new);
    }

}
