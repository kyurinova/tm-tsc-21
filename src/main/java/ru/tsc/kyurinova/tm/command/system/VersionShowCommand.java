package ru.tsc.kyurinova.tm.command.system;

import ru.tsc.kyurinova.tm.command.AbstractCommand;

public class VersionShowCommand extends AbstractCommand {

    @Override
    public String name() {
        return "version";
    }

    @Override
    public String arg() {
        return "-v";
    }

    @Override
    public String description() {
        return "Display program version...";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }
}
