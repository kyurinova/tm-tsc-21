package ru.tsc.kyurinova.tm.command.task;

import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Task;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-change-status-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Change status by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = serviceLocator.getTaskService().changeStatusById(userId, id, status);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}
