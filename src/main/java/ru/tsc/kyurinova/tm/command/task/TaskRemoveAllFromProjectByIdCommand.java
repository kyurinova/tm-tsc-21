package ru.tsc.kyurinova.tm.command.task;

import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.model.Project;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskRemoveAllFromProjectByIdCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "tasks-remove-from-project-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove tasks from project by id...";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(userId, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        serviceLocator.getProjectTaskService().removeAllTaskByProjectId(userId, projectId);
    }
}
